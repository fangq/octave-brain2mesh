Source: octave-brain2mesh
Maintainer: Qianqian Fang <fangqq@gmail.com>
Section: science
Priority: optional
Standards-Version: 4.5.0
Build-Depends: debhelper-compat (= 12), dh-octave
Homepage: https://mcx.space/brain2mesh
Vcs-Git: https://salsa.debian.org/pkg-octave-team/octave-brain2mesh.git
Vcs-Browser: https://salsa.debian.org/pkg-octave-team/octave-brain2mesh
Rules-Requires-Root: no


Package: octave-brain2mesh
Architecture: all
Depends: octave-iso2mesh, octave-image, ${octave:Depends}, ${misc:Depends}
Description: fully automated high-quality brain tetrahedral mesh generation toolbox
 The Brain2Mesh toolbox provides a streamlined matlab function to convert
 a segmented brain volumes and surfaces into a high-quality multi-layered
 tetrahedral brain/full head mesh. Typical inputs include segmentation
 outputs from SPM, FreeSurfer, FSL etc. This tool does not handle the
 segmentation of MRI scans, but examples of how commonly encountered
 segmented datasets can be used to create meshes can be found in the 
 package named brain2mesh-demos.


Package: matlab-brain2mesh
Section: contrib/science
Architecture: all
Depends: matlab-iso2mesh, matlab-support, ${misc:Depends}
Description: fully automated high-quality brain tetrahedral mesh generation toolbox
 Brain2Mesh is a fully automated 3D multi-layered brain mesh generator
 that supports both MATLAB and Octave. This package provides the toolbox
 for MATLAB. It requires the Image Processing Toolbox of MATLAB.

Package: brain2mesh-demos
Architecture: all
Depends: octave-brain2mesh, ${octave:Depends}, ${misc:Depends}
Description: sample files and demo scripts for Brain2Mesh
 This package contains sample JNIfTI sample data and meshing script 
 to demonstrate the basic functionalities of the Brain2Mesh Toolbox.
