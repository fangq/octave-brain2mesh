Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: brain2mesh
Upstream-Contact: fangqq@gmail.com
Source: https://github.com/fangq/brain2mesh
Comment:  The Brain2Mesh toolbox provides a streamlined matlab function 
 to convert a segmented brain volumes and surfaces into a high-quality 
 multi-layered tetrahedral brain/full head mesh.
 .
 The function `brain2mesh` handles the conversion of segmented volumes 
 into high-quality 3D meshes. It takes a 4D array as input, with 
 different assumptions as to the number of layers. Typically, the layers
 are assumed to contain: white matter (WM), grey matter (GM), 
 cerebrospinal fluid (CSF), bone and scalp. It also is able to handle 
 inputs missing a bone segmentation, and missing bone+scalp segmentation.
 .
 The function `brain1020` computes 10-20-like scalp landmarks with 
 user-specified density on a head mesh.

Files: *
Copyright: 2011,2017-2020 Qianqian Fang
License: GPL-3+

Files: intriangulation.m
Copyright: 2016 Johannes Korsawe
           2013 Adam H. Aitkenhead
License: BSD-4


Files: debian/*
Copyright: 2020 Qianqian Fang
           2020 Rafael Laboissière <rafael@debian.org>
License: GPL-3+

License: GPL-3+
 This program is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public
 License as published by the Free Software Foundation; either
 version 3 of the License, or (at your option) any later
 version.
 .
 This program is distributed in the hope that it will be
 useful, but WITHOUT ANY WARRANTY; without even the implied
 warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the GNU General Public License for more
 details.
 .
 You should have received a copy of the GNU General Public
 License along with this package; if not, write to the Free
 Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 Boston, MA  02110-1301 USA
 .
 On Debian systems, the full text of the GNU General Public
 License version 3 can be found in the file
 `/usr/share/common-licenses/GPL-3'.


License: BSD-4
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
 * Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution
 * Neither the name of Volkswagen AG nor the names of its
   contributors may be used to endorse or promote products derived from this
   software without specific prior written permission.
 * Neither the name of The Christie NHS Foundation Trust nor the names of its
   contributors may be used to endorse or promote products derived from this
   software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
